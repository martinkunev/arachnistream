#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import unicodescript
import string
import re

# TODO no ISO 639-1 code for berber and cherokee

# ISO 639-1 codes for languages
_SCRIPTS = {
	"Latin": {
		"en", "de", "nl", "da", "sv", "no", "af", "is", # germanic
		"fr", "es", "it", "pt", "ro", "ca", # romance
		"ga", # celtic
		"lt", "lv", # baltic
		"pl", "cs", "sk", "sl", "hr", # slavic
		"sq", # albanian
		"tg", "ku", # iranian
		"fi", "et", "hu", # uralic groups
		"eu", # basque
		"tr", "az", "uz", "kk", "tk", "tt", # turkic groups
		"ha", # chadic
		"so", "om", "aa", # cushitic
		"mt", # semitic
		"vi", # austroasiatic group
		"ms", "jv", "mg", "tl", "su", # austronesian group
		"yo", "ig", "ff", "rw", "zu", "rn", "sw", "xh", "tn", "wo", # niger-congo group
		"qu", # quechuan group
		"gn", # tupian group
	},
	"Cyrillic": {
		"ru", "uk", "be", "bg", "mk", "sr", # slavic
		"tg", "ku", # iranian
		"uz", "kk", "tk", "tt", "ky", "ba", "cv", # turkic groups
		"mn", # mongolic group
	},
	"Greek": {
		"el",
	},
	"Armenian": {
		"hy",
	},
	"Arabic": {
		"af", # germanic
		"fa", "ps", "ku", # iranian
		"ur", "pa", "sd", # indo-aryan
		"ar", # semitic
		"ha", # chadic
		"so", # cushitic
		"ug", # turkic groups
		"sw", # niger-congo group
	},
	"Hebrew": {
		"he", # semitic
		"yi", # germanic
	},
	"Ethiopic": {
		"am", "ti", # semitic
	},
	"Devanagari": {
		"hi", "mr", "sd", "ne", # indo-aryan
	},
	"Bengali": {
		"bn", # indo-aryan
	},
	"Gujarati": {
		"gu", # indo-aryan
	},
	"Gurmukhi": {
		"pa", "sd", # indo-aryan
	},
	"Telugu": {
		"te", # dravidian group
	},
	"Tamil": {
		"ta", # dravidian group
	},
	"Kannada": {
		"kn", # dravidian group
	},
	"Malayalam": {
		"ml", # dravidian group
	},
	"Sinhala": {
		"si", # indo-aryan
	},
	"Georgian": {
		"ka", # kartvelian group
	},
	"Mongolian": {
		"mn", # mongolic group
	},
	"Hangul": {
		"ko", # koreanic group
	},
	"Hiragana": {
		"ja", # japonic group
	},
	"Katakana": {
		"ja", # japonic group
	},
	"Han": {
		"zh", # sinitic
		"ja", # japonic group
	},
	"Khmer": {
		"km", # austroasiatic group
	},
	"Thai": {
		"th",
	},
	"Lao": {
		"lo",
	},
	"Tibetan": {
		"bo", "dz", # tibeto-burman
	},
	"Javanese": {
		"jv", "su", #
	},
	"Myanmar": {
		"my", # tibeto-burman
	},
	"Canadian_Aboriginal": {
		"iu",
	},
}

# ISO 3166-1 (countries) -> ISO 639-1 (languages)
_COUNTRIES = {
	"AF": ["ps", "fa"],
	"AL": ["sq"],
	"DZ": ["ar"],
	"AD": ["ca"],
	"AO": ["pt"],
	"AG": ["en"],
	"AR": ["es"],
	"AM": ["hy"],
	"AU": ["en"],
	"AT": ["de"],
	"AZ": ["az"],
	"BS": ["en"],
	"BH": ["ar"],
	"BD": ["bn"],
	"BB": ["en"],
	"BY": ["ru", "be"],
	"BE": ["fr", "nl", "de"],
	"BZ": ["en"],
	"BJ": ["fr"],
	"BT": ["dz"],
	"BO": ["es", "qu"],
	"BA": ["sr", "hr"],
	"BW": ["en", "tn"],
	"BR": ["pt"],
	"BN": ["ms", "en"],
	"BG": ["bg"],
	"BF": ["fr"],
	"BI": ["fr", "en"],
	"Cabo Verde CV": [""],
	"KH": ["km"],
	"CM": ["fr", "en"],
	"CA": ["en", "fr"],
	"CF": ["fr"],
	"TD": ["ar", "fr"],
	"CL": ["es"],
	"CN": ["zh"],
	"CO": ["es"],
	"KM": ["ar", "fr"],
	"CG": ["fr"],
	"CD": ["fr"],
	"CR": ["es"],
	"CI": ["fr"],
	"HR": ["hr"],
	"CU": ["es"],
	"CY": ["gr", "tr"],
	"CZ": ["cs", "sk"],
	"DK": ["da"],
	"DJ": ["ar", "fr"],
	"DM": ["en"],
	"DO": ["es"],
	"EC": ["es", "qu"],
	"EG": ["ar"],
	"SV": ["es"],
	"GQ": ["fr", "pt", "es"],
	"ER": ["ti"],
	"EE": ["et"],
	"SZ": ["en"],
	"ET": ["am", "ti", "so", "aa", "om", "en"],
	"FJ": ["en"],
	"FI": ["fi", "sv"],
	"FR": ["fr"],
	"GA": ["fr"],
	"GM": ["en"],
	"GE": ["ka"],
	"DE": ["de"],
	"GH": ["en"],
	"GR": ["gr"],
	"Greenland GL": [""],
	"GD": ["en"],
	"GT": ["es"],
	"GN": ["fr"],
	"GW": ["pt"],
	"GY": ["en"],
	"HT": ["fr"],
	"VA": ["it"],
	"HN": ["es"],
	"HK": ["zh", "en"],
	"HU": ["hu"],
	"IS": ["is"],
	"IN": ["hi", "en", "mr", "sd", "ne", "bn", "gu", "pa", "te", "ta", "kn", "ml", "si"],
	"ID": ["ms", "jv"],
	"IR": ["fa"],
	"IQ": ["ar"],
	"IE": ["en", "ga"],
	"IL": ["he"],
	"IT": ["it"],
	"JM": ["en"],
	"JP": ["ja"],
	"JO": ["ar"],
	"KZ": ["ru", "kk"],
	"KE": ["en", "sw"],
	"KI": ["en"],
	"KP": ["ko"],
	"KR": ["ko"],
	"KW": ["ar"],
	"KG": ["ru", "ky"],
	"LA": ["lo"],
	"LV": ["lv"],
	"LB": ["ar"],
	"LS": ["en"],
	"LR": ["en"],
	"LY": ["ar"],
	"LI": ["de"],
	"LT": ["lt"],
	"LU": ["fr", "de"],
	"MO": ["pt"],
	"MG": ["fr", "mg"],
	"MW": ["en"],
	"MY": ["ms", "en"],
	"ML": ["fr"],
	"MT": ["mt", "en"],
	"MR": ["ar"],
	"MU": ["en"],
	"MX": ["es"],
	"MD": ["ro", "ru"],
	"MC": ["fr"],
	"MN": ["mn"],
	"ME": ["sr"],
	"MA": ["ar"],
	"MZ": ["pt"],
	"MM": ["my"],
	"NA": ["en"],
	"NR": ["en"],
	"NP": ["ne"],
	"NL": ["nl"],
	"NC": ["fr"],
	"NZ": ["en"],
	"NI": ["es"],
	"NE": ["fr"],
	"NG": ["en", "ha", "yo", "ig"],
	"MK": ["mk", "sq"],
	"NO": ["no"],
	"OM": ["ar"],
	"PK": ["en", "ur"],
	"PW": ["en"],
	"PA": ["es"],
	"PG": ["en"],
	"PY": ["es", "gn"],
	"PE": ["es", "qu"],
	"PH": ["tl"],
	"PL": ["pl"],
	"PT": ["pt"],
	"PR": ["es"],
	"QA": ["ar"],
	"RO": ["ro"],
	"RU": ["ru"],
	"RW": ["en", "fr", "rw", "sw"],
	"KN": ["en"],
	"LC": ["en"],
	"VC": ["en"],
	"WS": ["en"],
	"SM": ["it"],
	"ST": ["pt"],
	"SA": ["ar"],
	"SN": ["fr", "wo"],
	"RS": ["sr"],
	"SC": ["en", "fr"],
	"SL": ["en"],
	"SG": ["en", "zh", "ms", "ta"],
	"SK": ["sk"],
	"SI": ["sl"],
	"SB": ["en"],
	"SO": ["ar"],
	"ZA": ["en", "af", "zu", "xh", "tn"],
	"SS": ["en"],
	"ES": ["es", "ca"],
	"LK": ["si", "ta"],
	"SD": ["ar", "en"],
	"SR": ["nl"],
	"SE": ["sv"],
	"CH": ["de", "fr", "it"],
	"SY": ["ar"],
	"TW": ["zh"],
	"TJ": ["tg"],
	"TZ": ["en", "sw"],
	"TH": ["th"],
	"TL": ["pt"],
	"TG": ["fr"],
	"TO": ["en"],
	"TT": ["en"],
	"TN": ["ar"],
	"TR": ["tr"],
	"TM": ["tk"],
	"TV": ["en"],
	"UG": ["en", "sw"],
	"UA": ["uk"],
	"AE": ["ar"],
	"GB": ["en"],
	"US": ["en"],
	"UY": ["es"],
	"UZ": ["uz"],
	"VU": ["en", "fr"],
	"VE": ["es"],
	"VN": ["vi"],
	"YE": ["ar"],
	"ZM": ["en"],
	"ZW": ["en", "tn", "xh"],
}

_NOUNS = {
	# hack: for words that correspond to languages, map to a country where that language is the only official one
	"england": "GB", "UK": "GB", "english": "GB", "british": "GB", "american": "GB", "US": "GB", "USA": "GB", "australia": "GB", "australian": "GB",
	"france": "FR", "french": "FR", "français": "FR", "française": "FR", "paris": "FR",
	"russia": "RU", "russian": "RU", "русский": "RU", "русская": "RU", "русское": "RU", "русские": "RU", "русском": "RU",
	"argentina": "AR", "chile": "AR", "mexico": "AR", "español": "AR", "castilian": "AR", "castellano": "AR",
	"spain": "ES", "spanish": "ES", "españa": "ES",
	"catalonia": "AD", "catalunya": "AD", "andorra": "AD",
	"arab": "EG", "arabic": "EG", "arabia": "EG", "UAE": "AE",
	"china": "CN", "chinese": "CN", "beijng": "CN", "shanghai": "CN", "taiwan": "TW", "taiwanese": "TW", "taipei": "TW",
	"germany": "DE", "deutschland": "DE", "german": "DE", "berlin": "DE", "austria": "AT", "österreich": "AT", "austrian": "AT", "vienna": "AT", "wien": "AT",
	"brazil": "BR", "brazilian": "BR", "brasil": "BR", "brasileiro": "BR", "brasileira": "BR", "brasileiros": "BR", "brasileiras": "BR", "portugal": "PT", "portuguese": "PT", "português": "PT", "portuguesa": "PT",
	"italy": "IT", "italian": "IT", "italiano": "IT", "italiana": "IT", "italiani": "IT", "italiane": "IT",
	"bulgaria": "BG", "bulgarian": "BG", "български": "BG", "българска": "BG", "българско": "BG",
	"greece": "GR", "greek": "GR", "athens": "GR",
	"angola": "AO",

	# "": "",
}

"""
Afghanistan AF
Albania AL
Algeria DZ
Andorra AD
Angola AO
Antigua and Barbuda AG
Argentina AR
Armenia AM
Australia AU
Austria AT
Azerbaijan AZ
Bahamas BS
Bahrain BH
Bangladesh BD
Barbados BB
Belarus BY
Belgium BE
Belize BZ
Benin BJ
Bermuda BM
Bhutan BT
Bolivia (Plurinational State of) BO
Bosnia and Herzegovina BA
Botswana BW
Brazil BR
Brunei Darussalam BN
Bulgaria BG
Burkina Faso BF
Burundi BI
Cabo Verde CV
Cambodia KH
Cameroon CM
Canada CA
Central African Republic CF
Chad TD
Chile CL
China CN
Colombia CO
Comoros KM
Congo CG
"Congo  Democratic Republic of the"
Costa Rica CR
Côte d'Ivoire CI
Croatia HR
Cuba CU
Cyprus CY
Czechia CZ
Denmark DK
Djibouti DJ
Dominica DM
Dominican Republic DO
Ecuador EC
Egypt EG
El Salvador SV
Equatorial Guinea GQ
Eritrea ER
Estonia EE
Eswatini SZ
Ethiopia ET
Fiji FJ
Finland FI
France FR
Gabon GA
Gambia GM
Georgia GE
Germany DE
Ghana GH
Greece GR
Greenland GL
Grenada GD
Guatemala GT
Guinea GN
Guinea-Bissau GW
Guyana GY
Haiti HT
Holy See VA
Honduras HN
Hong Kong HK
Hungary HU
Iceland IS
India IN
Indonesia ID
Iran (Islamic Republic of) IR
Iraq IQ
Ireland IE
Israel IL
Italy IT
Jamaica JM
Japan JP
Jordan JO
Kazakhstan KZ
Kenya KE
Kiribati KI
Korea (Democratic People's Republic of) KP
"Korea  Republic of"
Kuwait KW
Kyrgyzstan KG
Lao People's Democratic Republic LA
Latvia LV
Lebanon LB
Lesotho LS
Liberia LR
Libya LY
Liechtenstein LI
Lithuania LT
Luxembourg LU
Macao MO
Madagascar MG
Malawi MW
Malaysia MY
Maldives MV
Mali ML
Malta MT
Mauritania MR
Mauritius MU
Mexico MX
Micronesia (Federated States of) FM
"Moldova  Republic of"
Monaco MC
Mongolia MN
Montenegro ME
Morocco MA
Mozambique MZ
Myanmar MM
Namibia NA
Nauru NR
Nepal NP
Netherlands NL
New Caledonia NC
New Zealand NZ
Nicaragua NI
Niger NE
Nigeria NG
North Macedonia MK
Norway NO
Oman OM
Pakistan PK
Palau PW
"Palestine  State of"
Panama PA
Papua New Guinea PG
Paraguay PY
Peru PE
Philippines PH
Poland PL
Portugal PT
Puerto Rico PR
Qatar QA
Romania RO
Russian Federation RU
Rwanda RW
Saint Barthélemy BL
Saint Kitts and Nevis KN
Saint Lucia LC
Saint Vincent and the Grenadines VC
Samoa WS
San Marino SM
Sao Tome and Principe ST
Saudi Arabia SA
Senegal SN
Serbia RS
Seychelles SC
Sierra Leone SL
Singapore SG
Slovakia SK
Slovenia SI
Solomon Islands SB
Somalia SO
South Africa ZA
South Sudan SS
Spain ES
Sri Lanka LK
Sudan SD
Suriname SR
Sweden SE
Switzerland CH
Syrian Arab Republic SY
"Taiwan  Province of China"
Tajikistan TJ
"Tanzania  United Republic of"
Thailand TH
Timor-Leste TL
Togo TG
Tonga TO
Trinidad and Tobago TT
Tunisia TN
Turkey TR
Turkmenistan TM
Tuvalu TV
Uganda UG
Ukraine UA
United Arab Emirates AE
United Kingdom of Great Britain and Northern Ireland GB
United States of America US
Uruguay UY
Uzbekistan UZ
Vanuatu VU
Venezuela (Bolivarian Republic of) VE
Viet Nam VN
Western Sahara EH
Yemen YE
Zambia ZM
Zimbabwe ZW
"""

def language(title):
	def _languages_script(title):
		scripts = []
		nonlatin = set()
		for letter in title:
			script = unicodescript.script(letter)
			if script == "Common":
				continue
			scripts.append(script)
			if script != "Latin":
				nonlatin.add(script)

		length = len(nonlatin)
		if length == 0:
			# TODO look at accent characters
			return set(_SCRIPTS["Latin"])
		elif length == 1:
			# TODO for cyrillic, look for specific letters
			return set(_SCRIPTS[nonlatin.pop()])
		else:
			return set()

	def _languages_country(title):
		languages = []
		for word in title.split(" "):
			word = re.sub(r"[_'\"`.,;:?!/\\<>()\[\]{}|~@#$%^&*=+-]", "", word.lower())
			if word in _NOUNS:
				languages.append(set(_COUNTRIES[_NOUNS[word]]))
		return set().union(*languages)

		languages_script = _languages_script(title)
		if len(languages_script) == 1:
			return languages_script[0]

	languages_script = _languages_script(title)
	if len(languages_script) == 1:
		return languages_script.pop()

	languages_country = _languages_country(title)

	if len(languages_country) == 1:
		return languages_country.pop()
	elif languages_script and languages_country:
		intersection = languages_script & languages_country
		if len(intersection) == 1:
			return intersection.pop()

	return ""
