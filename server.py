#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import http.server
import urllib
from datetime import datetime
import time
import json
import pystache

from lib import *

class segment_context():
	def __init__(self):
		self.context = [{}]

	def push(self):
		self.context.append(self.context[-1].copy())

	def pop(self):
		self.context.pop()

	def get(self, attribute=None):
		if attribute:
			return self.context[-1][attribute]
		else:
			return self.context[-1]

	def set(self, attribute, value):
		self.context[-1][attribute] = value

	def init(self, nodes):
		for node in nodes:
			if node.tag == "SegmentTemplate":
				self.set("template", node)
				# TODO there may not be duration and timescale; in this case we're looking for SegmentTimeline
			elif node.tag == "SegmentList":
				self.set("list", node)
			elif node.tag == "BaseURL":
				self.set("url", node)
			elif node.tag == "UTCTiming":
				self.set("timing", node)

def dash_process(net, mpd):
	# TODO pass headers to download

	def dash_stream_template(client, entity, init, segment, index, length):
		code, data, _ = net.download(client, entity + init)
		if code >= 400:
			raise EOFError
		yield data

		while True:
			code, data, _ = net.download(client, entity + segment.replace("$Number$", str(index)))
			if code >= 400:
				time.sleep(length)
				continue
			yield data
			index += 1

	def dash_stream_list(client, entity, segments, length):
		while len(segments):
			code, data, _ = net.download(client, entity + segments[0])
			if code >= 400:
				time.sleep(length)
				continue
			segments.pop(0)
			yield data

	tree = XML(mpd)

	representations = {}
	ctx = segment_context()

	ctx.init(tree.children())
	ctx.push()

	# TODO handle multiple adaptation sets
	"""sets = tree.nodes("Period[1]", "AdaptationSet")
	for s in sets:
		print(s.get("mimeType"))"""

	ctx.init(tree.children("Period[1]", "AdaptationSet[1]"))
	# TODO adaptation set get 

	content = tree.nodes("Period[1]", "AdaptationSet[1]", "Representation")
	for node in content:
		ctx.push()
		ctx.init(node.children())
		ctx.set("bandwidth", node.get("bandwidth"))
		representations[node.get("id")] = ctx.get()
		ctx.pop()

	ctx.pop()

	representation_id = None
	for key in representations.keys():
		# TODO allow selecting from all representations
		representation_id = key
		break
	if not representation_id:
		return # no representations found
	representation = representations[representation_id]

	# Initialize request parameters.
	base = urllib.parse.urlparse(representation["url"].text)
	client = net._client(base.scheme, base.netloc)
	entity = net._entity_name(base)

	try:
		if "template" in representation:
			template = representation["template"]
			init = template.get("initialization").replace("$RepresentationID$", representation_id)
			media = template.get("media").replace("$RepresentationID$", representation_id)

			# TODO there may not be duration and timescale; in this case we're looking for SegmentTimeline
			length = int(template.get("duration")) / int(template.get("timescale"))

			#publish = int(datetime.fromisoformat(tree._tree.get("publishTime").replace("Z", "+00:00")).timestamp())

			now, _ = net.download_text(representation["timing"].get("value"), None, html_only=False)
			now = int(datetime.fromisoformat(now.replace("Z", "+00:00")).timestamp())
			number = int(now // length)

			return length, dash_stream_template(client, entity, init, media, number, length)
		elif "list" in representation:
			segment_list = representation["list"]

			length = 0 # TODO get segment length
			segments = [segment.get("media") for segment in segment_list.children()]
			return 0, dash_stream_list(client, entity, segments, length)
	finally:
		client.close()

"""https://www.brendanlong.com/the-structure-of-an-mpeg-dash-mpd.html
for a stream, I would expect to have 1 Period
1 AdaptationSet for each media - e.g. audio stream, video stream, subtitles. I could extract language from the audio stream metadata
Representation - different bandwidths, resolutions, codecs, etc. for the same content
media segments - actual content. 3 ways:
	* BaseURL for a single-segment Representation
	* SegmentList
	* SegmentTemplate
	SegmentBase - info applying to all segments
	SegmentTimeline - for describing start times and durations
	if at higher level, applies to all subsections
index segments - for media segment metadata; 2 types:
	* Representation Index Segment - for entire representation (a separate file)
	* Single Index Segment - per media segment
"""

mpd_test = """<?xml version="1.0" encoding="utf-8"?>
<MPD xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:mpeg:dash:schema:mpd:2011" xmlns:dvb="urn:dvb:dash:dash-extensions:2014-1"
  xsi:schemaLocation="urn:mpeg:dash:schema:mpd:2011 http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-DASH_schema_files/DASH-MPD.xsd"
  type="dynamic" availabilityStartTime="1969-12-31T23:59:44Z"
  minimumUpdatePeriod="PT6H" timeShiftBufferDepth="PT6H" maxSegmentDuration="PT7S" minBufferTime="PT3.200S"
  profiles="urn:dvb:dash:profile:dvb-dash:2014,urn:dvb:dash:profile:dvb-dash:isoff-ext-live:2014"
  publishTime="2020-06-18T12:52:32">

    <UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-iso:2014" value="https://time.akamai.com/?iso" />

    <BaseURL dvb:priority="1" dvb:weight="1" serviceLocation="llnws">https://as-dash-ww-live.bbcfmt.s.llnwi.net/pool_904/live/ww/bbc_world_service/bbc_world_service.isml/dash/</BaseURL>

    <Period id="1" start="PT0S">
		<AdaptationSet group="1" contentType="audio" lang="en" minBandwidth="48000" maxBandwidth="96000"
					   segmentAlignment="true" audioSamplingRate="48000" mimeType="audio/mp4" codecs="mp4a.40.5" startWithSAP="1">
			<UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-iso:2014" value="https://time.akamai.com/?iso" />
			<AudioChannelConfiguration schemeIdUri="urn:mpeg:dash:23003:3:audio_channel_configuration:2011" value="2"/>
			<Role schemeIdUri="urn:mpeg:dash:role:2011" value="main"/>
			<SegmentTemplate timescale="48000" initialization="bbc_world_service-$RepresentationID$.dash"
						   media="bbc_world_service-$RepresentationID$-$Number$.m4s" startNumber="1" duration="307200"/>
			<Representation id="audio=48000" bandwidth="48000"/>
			<Representation id="audio=96000" bandwidth="96000"/>
		</AdaptationSet>
    </Period>
</MPD>
"""

class Server(http.server.BaseHTTPRequestHandler):
	def _list(self, search=""):
		links = []

		index = 0
		for stream in self.streams:
			info = stream["tags"]["title"] if ("title" in stream["tags"]) else stream["url"]
			if not info:
				info = stream["url"]
			language = stream["tags"]["language"] if ("language" in stream["tags"]) else ""
			content = stream["content_type"]
			#reliability = "{}% {}s ({}s - {}s)".format(round(stream["availability"] * 100), round(stream["delay_avg"], 2), round(stream["delay_min"], 2), round(stream["delay_max"], 2))
			reliability = "{}s ({}s - {}s)".format(round(stream["delay_avg"], 2), round(stream["delay_min"], 2), round(stream["delay_max"], 2))
			if (info.lower().find(search.lower()) >= 0) or (language.lower() == search.lower()):
				links.append({"path": str(index), "title": stream["url"], "name": info, "language": language, "content": content, "tags": stream["tags"], "reliability": reliability})
			index += 1

		template = pystache.Renderer(file_encoding="UTF-8")
		body = template.render_path("index.html", {"stream": links, "search": search})
		body = bytes(body, encoding="UTF-8")

		self.send_response(200)
		self.send_header("content-type", "text/html")
		self.send_header("content-length", str(len(body)))
		self.end_headers()
		self.wfile.write(body)

	_SEARCH = "search?"

	def do_GET(self):
		self.protocol_version = "HTTP/1.1"

		url = self.path[1:]
		if not url:
			return self._list()

		mpd = ""

		if url.startswith(self._SEARCH):
			query = urllib.parse.parse_qs(url[len(self._SEARCH):])
			return self._list(query["title"][0] if ("title" in query) else "")
		elif url == "bbc":
			mpd, _ = self.net.download_text("https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/llnws/bbc_world_service.mpd", None, html_only=False)
		#elif url == "test":
		#	mpd, _ = self.net.download_text("https://manifest.googlevideo.com/api/manifest/dash/expire/1614566721/ei/4QA8YIP8L8XnxgKMt4OIAg/ip/88.162.255.29/id/SebJIjQgdjg.1/source/yt_live_broadcast/requiressl/yes/tx/24005204/txs/24005203%2C24005204%2C24005205%2C24005206%2C24005207%2C24005208%2C24005209/as/fmp4_audio_clear%2Cwebm_audio_clear%2Cwebm2_audio_clear%2Cfmp4_sd_hd_clear%2Cwebm2_sd_hd_clear/vprv/1/pacing/0/keepalive/yes/itag/0/playlist_type/LIVE/sparams/expire%2Cei%2Cip%2Cid%2Csource%2Crequiressl%2Ctx%2Ctxs%2Cas%2Cvprv%2Citag%2Cplaylist_type/sig/AOq0QJ8wRQIgKtspF8OHAMuS_Vttk1yJjBahqr2EtQcc8PLDKPNq8cICIQD5EdLV3z77hK2YtPL_Qn3FToLVjKarrKMFMu_477EXXg%3D%3D", None, html_only=False)
		#	#mpd = mpd_test
		elif url.isdigit():
			index = int(url)
			if index >= len(self.streams):
				self.send_error(404)
				return

			# Handle dash requests and redirect everything else.
			stream = self.streams[index]
			if stream["content_type"] in {"application/dash+xml", "video/vnd.mpeg.dash.mpd"}:
				#print(stream["url"], stream["tags"])
				headers = {"referer": stream["tags"]["referer"]} if "referer" in stream["tags"] else {}
				mpd, headers = self.net.download_text(stream["url"], None, headers, html_only=False)
				#print(mpd, headers)
				if "ignore" in headers:
					print("download error", headers["ignore"])
					return
			else:
				self.send_response(307)
				self.send_header("location", stream["url"]) # TODO headers - e.g. referer
				self.send_header("content-length", "0")
				self.end_headers()
				return
		else:
			self.send_error(404)
			return

		#print(mpd)
		length, stream = dash_process(self.net, mpd)

		# Send content using chunked encoding.
		self.send_response(200)
		self.send_header("content-type", "audio/mp4") # TODO get encoding from adaptation set
		self.send_header("transfer-encoding", "chunked")
		self.end_headers()
		progress = 0
		for chunk in stream:
			try:
				size = bytes(hex(len(chunk))[2:], encoding="ASCII")
				terminator = bytes("\r\n", encoding="ASCII")
				self.wfile.write(size + terminator + chunk + terminator)
			except BrokenPipeError:
				break
			progress += length

def listen(net, streams):
	Server.net = net
	Server.streams = streams
	httpd = http.server.HTTPServer(("127.0.0.1", 8080), Server)
	httpd.serve_forever()
