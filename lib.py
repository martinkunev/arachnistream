#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import io
import xml.etree.ElementTree as xml

DEBUG = True

def log(*args, important=False):
	if important or DEBUG:
		print(*args)

class XML():
	def __init__(self, document, path=None, item=None):
		if type(document) == str:
			assert not path, "invalid initializer"

			self._tree = xml.fromstring(document)
			xml_stream = io.BytesIO(bytes(document, "UTF-8"))

			self._namespaces = {node[0]: node[1] for _, node in xml.iterparse(xml_stream, events=["start-ns"])}
			for key, value in self._namespaces.items():
				xml.register_namespace(key, value)
			self._ns = "{" + self._namespaces[""] + "}"

			self._path = ()
		else:
			assert path, "invalid initializer"

			self._tree = document._tree
			self._ns = document._ns
			self._path = path

			self._item = item
			self.tag = path[-1]
			index = self.tag.find("[")
			if index >= 0:
				self.tag = self.tag[:index]
			self.text = item.text

	def _ns_prefix(self, path):
		path = map(lambda tag: self._ns + tag, path)
		return "/".join(path)

	def nodes(self, *name):
		path = self._ns_prefix(self._path + name)

		result = []
		index = 0
		for item in self._tree.findall(path):
			index += 1
			if not item.tag.startswith(self._ns):
				continue

			last = name[-1]
			if path[-1] == "]":
				return XML(self, self._path + name, item)
			else:
				last += "[" + str(index) + "]"
				xml = XML(self, self._path + name[:-1] + (last,), item)
				result.append(xml)
		return result

	def children(self, *name):
		path = "./" + self._ns_prefix(self._path + name) + "/"

		result = []
		indices = {}
		for item in self._tree.findall(path):
			if not item.tag.startswith(self._ns):
				continue
			tag = item.tag[len(self._ns):]

			if tag in indices:
				indices[tag] += 1
			else:
				indices[tag] = 1

			last = tag + "[" + str(indices[tag]) + "]"
			xml = XML(self, self._path + name + (last,), item)
			result.append(xml)
		return result

	def get(self, attribute):
		return self._item.get(attribute)
