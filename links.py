#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import traceback
from urllib.parse import urlparse
from html.parser import HTMLParser
import html

class HTML(HTMLParser):
	def __init__(self, url, document):
		super().__init__()
		self.links = {}

		parsed = urlparse(url)
		self.protocol = parsed.scheme
		self.hostname = parsed.netloc
		path = parsed.path if parsed.path else "/"
		path = path[:path.rfind("/")+1]
		self.base = parsed.scheme + "://" + parsed.netloc + path

		# Initialize regular expression for URLs. Take into account the following:
		# A URL can contain a query string.
		# The slashes could be escaped (if in javascript).
		schema = r"\b(?:https?:)\\?/\\?/"
		host = r"(?:[a-z0-9.-]+)"
		port = r"(?::(?:\d+))?"
		path = r"(?:\\?/[^?#\"'\s]*)"
		query = r"(?:\\?[a-z0-9._=-]*)?"
		anchor = r"(?:#[a-z0-9._-]+)?\b"
		self.pattern = schema + host + port + path + query + anchor

		self._in_title = False
		self.title = ""
		self.js = False
		self.anchor = None

		self.feed(document)

	def handle_starttag(self, tag, attributes):
		def attribute(name):
			for entry in attributes:
				if entry[0] == name:
					return entry[1]

		if tag == "base":
			self.base = attribute("href")
			if self.base and (self.base[-1] != "/"):
				self.base += "/"
			return
		#elif self.tags and (tag not in self.tags):
		#	return

		if tag == "title":
			self._in_title = True
		elif tag == "iframe":
			link = attribute("src")
			if not link:
				return
			self._add(link, {"element": "iframe"}, self.title)
		elif tag == "video":
			link = attribute("src")
			if not link:
				return
			self._add(link, {"element": "video"}, self.title)
		elif tag == "a":
			link = attribute("href")
			if not link:
				return
			if (link[0] == "#"):
				return # ignore anchors
			self.anchor = link
		elif tag == "span":
			link = attribute("data-clipboard-text") # used by hlscat.com
			if not link:
				return
			if not re.match(self.pattern, link, re.IGNORECASE):
				print("pattern mismatch: ", link)
				return
			self._add(link, {"element": ""}, self.title)
		elif tag == "noscript":
			self.js = True

	def handle_endtag(self, tag):
		if tag == "title":
			self._in_title = False
		elif self.anchor:
			self._add(self.anchor, {"element": "a"}, self.title)
			self.anchor = None

	def handle_data(self, data):
		if self._in_title:
			self.title = data
			return

		playlist = data.find("#EXTM3U")
		if playlist >= 0:
			playlist = data[playlist:].split("\n")
			print("playlist", playlist)

			title = ""
			for line in playlist:
				if line.startswith("#PLAYLIST:"):
					title = line.split(":")[1]
				elif line.startswith("#EXTINF:"):
					title = line.split(":")[1].split(",")[-1]
				elif line.startswith("#EXTGRP:"):
					pass
				else:
					found = re.findall(self.pattern, data, re.IGNORECASE)
					print(found)
					for entry in found:
						self._add(entry, {"element": ""}, title)
		else:
			if self.anchor:
				self._add(self.anchor, {"element": "a"}, data)
				self.anchor = None

			# TODO will this catch javascript?
			found = re.findall(self.pattern, data, re.IGNORECASE)
			for entry in found:
				self._add(entry, {"element": "text"}, self.title)

	def _add(self, link, info, title):
		# TODO do I need the check below
		#if not all(31 < ord(c) < 128 for c in link):
		#	print("skip invalid URL " + link)
		#	return

		if re.match(r"^[^h][a-z]*:", link, re.IGNORECASE):
			return # ignore protocols not starting with h

		link = link.replace("\\", "").replace(" ", "%20")

		if link.startswith("http://") or link.startswith("https://"):
			pass
		elif link.startswith("//"):
			link = self.protocol + "://" + link[2:]
		elif link.startswith("/"):
			link = self.protocol + "://" + self.hostname + link
		else:
			link = self.base + link

		info["title"] = html.escape(title)
		self.links[link] = info
