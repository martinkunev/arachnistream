#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import sqlite3
import json

_connection = None

def connect():																																				 
	global _connection																																		 
	_connection = sqlite3.connect('arachni.db')
	_execute("CREATE TABLE IF NOT EXISTS links(url text unique, tags text, content_type text)")
	_execute("CREATE TABLE IF NOT EXISTS state(url text, available bool, delay real, date text)")

def _execute(query, *args):
	c = _connection.cursor()
	c.execute(query, args)
	_connection.commit()

def save_url(url, tags, content_type=None):
	tags = json.dumps(tags, ensure_ascii=False)
	if content_type:
		_execute("INSERT INTO links(url,tags,content_type) VALUES(?,?,?) ON CONFLICT(url) DO UPDATE SET tags=?,content_type=?", url, tags, content_type, tags, content_type)
	else:
		_execute("INSERT INTO links(url,tags) VALUES(?,?) ON CONFLICT(url) DO UPDATE SET tags=?", url, tags, tags)

def save_state(url, available, date="", delay=None):
	if not available:
		date = ""
		delay = None
	_execute("INSERT INTO state(url,available,delay,date) VALUES(?,?,?,?)", url, available, delay, date)

def load():
	query = """
	SELECT links.url,links.tags,links.content_type,state.low,state.high,state.wait,state.availability,state.total,state.last FROM
		links INNER JOIN (SELECT
			url,MIN(delay) AS low,MAX(delay) AS high,SUM(delay) AS wait,SUM(available) AS availability,COUNT(available) AS total,MAX(date) AS last
			FROM state GROUP BY url) AS state
		ON links.url = state.url
		ORDER BY state.last ASC"""

	c = _connection.cursor()
	entries = c.execute(query).fetchall()
	return [{
		"url": entry[0],
		"tags": json.loads(entry[1]),
		"content_type": entry[2],
		"delay_min": entry[3],
		"delay_max": entry[4],
		"delay_avg": entry[5] / entry[6],
		"availability": entry[6] / entry[7],
		"count": entry[7],
		"last": entry[8],
	} for entry in entries]

def disconnect():
	_connection.close()
