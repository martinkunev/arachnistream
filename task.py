#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import time
import re
from urllib.parse import urlparse

import storage
from links import HTML
import countries
import network

TASK_CRAWL = 0
TASK_HEARTBEAT = 1

# TODO better js support - squidtv uses bloburls (generated at runtime). some sites use an onclick event to load a video
# TODO try to ignore useless content - images, css, etc.
# TODO store referer tag for each link so that if the link changes the old version can be replaced with the new (by comparing referer)
# TODO ignore ssl warnings?

"""youtube:
./youtube-dl --list-formats 'https://www.youtube.com/watch?v=SebJIjQgdjg'
select format code from response (e.g. 95):
./youtube-dl -f 95 -g 'https://www.youtube.com/watch?v=SebJIjQgdjg'
result is mpd

youtube-nocookie.com
"""

"""
interesting content types
audio/mpeg						rfi audio
application/vnd.apple.mpegurl	nova tv
application/dash+xml			dash mpd
video/vnd.mpeg.dash.mpd			youtube dash mpd
application/x-mpegurl			hlscat
video/mp2t						encrypted video (whatever that means)
"""

class Task():
	_EXTENSIONS = {"mp3", "m3u", "m3u8", "mpd", "ts"}
	_TYPES = {"audio/mpeg", "application/vnd.apple.mpegurl", "application/dash+xml", "video/vnd.mpeg.dash.mpd", "application/x-mpegurl", "audio/x-mpegurl", "video/mp4"}

	def __init__(self, action, url, depth=0, headers=None, *, info=None):
		self.url = re.sub(r"#.*", "", url)
		self.depth_left = depth
		self.headers = headers if headers else {}
		if action == TASK_CRAWL:
			self.action = lambda net, client: self._crawl(net, client)
		elif action == TASK_HEARTBEAT:
			self.action = self._heartbeat

		self.parsed = urlparse(url)
		self.connect_id = self.parsed.scheme + "://" + self.parsed.netloc
		path = self.parsed.path if self.parsed.path else "/"
		self.entity = path + ("?" + self.parsed.query if self.parsed.query else "")

		if not info:
			info = {"element": ""}
		self.info = info

	def __str__(self):
		return ("HEAD" if (self.action == self._heartbeat) else "GET") + " " + self.url

	def done(self, delay):
		pass # TODO write to storage

	def _link_ignore(self, link, netloc, info):
		lds = netloc.split(".")
		if len(lds) < 2:
			return False

		if lds[-2] in {"instagram", "facebook", "twitter"}:
			return True

		if len(lds) < 3:
			return False
		blds = lds[:-2] # bottom level domains
		return ("ad" in blds) or ("ads" in blds) or ("googleads" in blds)

	def _crawl(self, net, client):
		"""Finds URLs in a document (with optional filtering)."""

		def extension(url):
			groups = re.match(r".*\.([a-z0-9]+)$", url, re.IGNORECASE)
			return groups[1] if groups else ""

		js = (self.info["element"] == "iframe")
		document, headers = net.download_text(self.url, client, self.headers, js=js, html_only=False)
		if not document:
			if ("code" in headers) and (headers["code"] < 300):
				self._heartbeat_process(net, headers["code"], headers["headers"], 0) # TODO set delay value properly
			return
		html = HTML(self.url, document)

		# Check if we should retry with javascript enabled.
		if (not js) and html.js:
			document, err = net.download_text(self.url, client, self.headers, js=True)
			if not document:
				print("unable to download " + self.url, err)
				return
			html = HTML(self.url, document)

		language = countries.language(html.title)

		headers = self.headers.copy()
		headers.update({"referer": self.url})
		for link in html.links:
			info = html.links[link]
			parsed = urlparse(link)
			link_netloc = parsed.netloc

			if self._link_ignore(link, link_netloc, info):
				#print("ignoring", link)
				continue

			info["referer"] = self.url

			#print(extension(parsed.path), link)
			if extension(parsed.path) in self._EXTENSIONS:
				if language:
					info["language"] = language

				entry = Task(TASK_HEARTBEAT, link, headers=headers, info=info)
				net.enqueue(entry)
			elif link_netloc.endswith("youtu.be") or link_netloc.endswith("youtube.com"):
				link = net.youtube_stream(link, {}, None)
				if not link:
					continue

				info["content-type"] = "application/dash+xml"
				if language:
					info["language"] = language

				entry = Task(TASK_HEARTBEAT, link, headers=headers, info=info)
				net.enqueue(entry)
			elif info["element"] == "iframe":
				entry = Task(TASK_CRAWL, link, self.depth_left, headers=headers, info=info)
				net.enqueue(entry)
			elif self.depth_left:
				#if link_netloc != self.parsed.netloc:
				#	lib.log("skip cross-domain link " + link)
				#	continue

				entry = Task(TASK_CRAWL, link, self.depth_left - 1, headers=headers, info=info)
				net.enqueue(entry)

	def _heartbeat_process(self, net, status, headers, delay):
		mime = net.content_type(headers)
		if (mime in self._TYPES) or (mime.find("audio") >= 0) or (mime.find("video") >= 0):
			if mime not in self._TYPES:
				print("new mime: " + mime)
			storage.save_url(self.url, self.info, mime)
			storage.save_state(self.url, status < 400, net.date(headers), delay)
			print("LINK write", self.url, self.info, headers["content-type"])

	def _heartbeat(self, net, client):
		"""Checks if a URL is accessible."""

		storage.save_url(self.url, self.info)

		start = time.time()
		status, headers = net.head(client, self.entity, self.headers)
		if status >= 400:
			return
		delay = time.time() - start

		self._heartbeat_process(net, status, headers, delay)
