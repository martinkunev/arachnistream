#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import time
#import queue.PriorityQueue as heap
import http.client
import ssl
from requests_html import HTMLSession
import urllib
import traceback
import json
import dateutil.parser as dateparse

import lib

class Network():
	_TIME = 0
	_CLIENT = 1
	_QUEUE = 2
	_ROBOTS = 3

	_TIMEOUT = 20

	def __init__(self):
		self.locations = {} # one per "protocol:host:port"
		self.processed = set()
		self.session = None

	def enqueue(self, task):
		if task.url in self.processed:
			return
		self.processed.add(task.url)

		#print("enqueue link " + task.url)
		if task.connect_id in self.locations:
			self.locations[task.connect_id][self._QUEUE].append(task)
		else:
			self.locations[task.connect_id] = [0, None, [task], None]

	def _connect(self, connect_id):
		try:
			secure, host, port = re.match(r"^http(s?)://([^:]+):?(\d*)$", connect_id).groups()

			#lib.log("https" if secure else "http ", host, port, path)
			if secure:
				port = int(port) if port else 443
				return http.client.HTTPSConnection(host, port, timeout=self._TIMEOUT, context=ssl._create_unverified_context())
			else:
				port = int(port) if port else 80
				return http.client.HTTPConnection(host, port, timeout=self._TIMEOUT)
		except:
			return None

	def _robots(self, client):
		try:
			code, body, headers = self.download(client, "/robots.txt")
			if code >= 400:
				return []

			AGENT = "user-agent:"
			ALLOW = "allow:"
			DISALLOW = "disallow:"

			rules = {}

			ignore = True
			for line in str(body, "ASCII").split("\n"):
				line = line.strip()
				lower = line.lower()
				if lower.startswith(AGENT):
					ignore = (line[len(AGENT):].strip() != "*")
				elif ignore:
					continue
				elif lower.startswith(ALLOW):
					if "disallow" in rules:
						return {} # invalid robots
					if "allow" not in rules:
						rules["allow"] = []
					rules["allow"].append(line[len(ALLOW):].strip())
				elif lower.startswith(DISALLOW):
					if "allow" in rules:
						return {} # invalid robots
					if "disallow" not in rules:
						rules["disallow"] = []
					path = line[len(DISALLOW):].strip()
					if path:
						rules["disallow"].append(path)
		except Exception as ex:
			pass

		#lib.log(client.host, client.port, body, rules)

		return rules

	def _robots_allow(self, location, entity):
		if "allow" in location[self._ROBOTS]:
			for rule in location[self._ROBOTS]["allow"]:
				if entity.startswith(rule):
					return True
			return False
		elif "disallow" in location[self._ROBOTS]:
			for rule in location[self._ROBOTS]["disallow"]:
				if entity.startswith(rule):
					return False

		return True

	def execute_step(self):
		def priority(item):
			index, connect_id = item
			return -self.locations[connect_id][self._TIME]

		locations = sorted(enumerate(self.locations), key=priority)
		for index, connect_id in locations:
			# Make sure there is a client available.
			location = self.locations[connect_id]
			if not location[self._CLIENT]:
				location[self._CLIENT] = self._connect(connect_id)
				if not location[self._CLIENT]:
					continue

				if location[self._ROBOTS] == None:
					location[self._ROBOTS] = self._robots(location[self._CLIENT])

			# Take a task from the queue.
			queue = location[self._QUEUE]
			assert queue
			task = queue.pop(0)
			if not queue:
				del self.locations[connect_id]

			if not self._robots_allow(location, task.entity):
				lib.log("not allowed", task.entity)
				continue

			# Wait to not overload the server and remember execution start time for the task.
			start = time.time()
			waited = start - location[self._TIME]
			if waited < 1:
				delay = 1 - waited
				time.sleep(delay)
				start += delay

			#print("executing task " + str(task) + " " + connect_id)
			try:
				task.action(self, location[self._CLIENT])
			except Exception as ex:
				# TODO if connection error, reconnect and retry?
				traceback.print_exc()
				location[self._CLIENT] = None

			now = time.time()
			location[self._TIME] = now
			task.done(now - start)

		return len(self.locations) != 0

	def _client(self, scheme, netloc):
		connect_id = scheme + "://" + netloc
		if connect_id in self.locations:
			location = self.locations[connect_id]
			client = location[self._CLIENT]
			location[self._TIME] = time.time()
			if client:
				return client
		return self._connect(connect_id)

	def _entity_name(self, parsed):
		path = parsed.path if parsed.path else "/"
		return path + ("?" + parsed.query if parsed.query else "")

	def content_type(self, headers):
		if "content-type" not in headers:
			return "application/octet-stream"
		return headers["content-type"].split(";")[0]

	def date(self, headers):
		if "date" not in headers:
			return ""
		return dateparse.parse(headers["date"]).isoformat()

	def head(self, client, entity, headers=None):
		if headers == None:
			headers = {}

		while True:
			try:
				lib.log("HEAD", ("https://" if client.default_port == 443 else "http://"), client.host, client.port, entity, headers)
				client.request("HEAD", entity, headers=headers)
				response = client.getresponse()
				response.read()
				if (response.status // 100) == 3:
					url = response.headers["location"]
					parsed = urllib.parse.urlparse(url)
					if parsed.netloc: # netloc can be empty when redirecting to the same server
						client = self._client(parsed.scheme, parsed.netloc)
					entity = self._entity_name(parsed)
				else:
					return response.status, response.headers
			except http.client.RemoteDisconnected:
				print("head remote disconnect", client.host, client.port, entity, headers)
				traceback.print_exc()
				return 500, None # TODO see if there is a better way to deal with this
			except http.client.InvalidURL:
				lib.log("invalid URL", entity)
				return 400, None
			except UnicodeEncodeError:
				lib.log("invalid content encoding") # TODO can this happen
				return 500, None
			except Exception as ex:
				return 500, None

	def download(self, client, entity, headers=None):
		if headers == None:
			headers = {}

		while True:
			try:
				lib.log("GET", ("https://" if client.default_port == 443 else "http://"), client.host, client.port, entity, headers)
				client.request("GET", entity, headers=headers)
				response = client.getresponse()
				body = response.read()
				if response.status >= 400:
					return response.status, None, None
				elif response.status >= 300:
					url = response.headers["location"]
					parsed = urllib.parse.urlparse(url)
					if parsed.netloc: # netloc can be empty when redirecting to the same server
						client = self._client(parsed.scheme, parsed.netloc)
					entity = self._entity_name(parsed)
				else:
					return response.status, body, response.headers
			except http.client.RemoteDisconnected:
				print("download remote disconnect", client.host, client.port, entity, headers)
				return 500, None, None # TODO see if there is a better way to deal with this
			except http.client.InvalidURL:
				lib.log("invalid URL", entity)
				return 400, None, None
			except UnicodeEncodeError:
				lib.log("invalid content encoding") # TODO can this happen
				return 500, None, None
			except Exception as ex:
				return 500, None, None

	def download_text(self, url, client, headers=None, js=False, html_only=True):
		"""Downloads a text document at a given URL. Stores it in cache and returns the result as a string. Optionally, renders javascript."""

		ALLOWED = {"text/html", "text/javascript", "text/xml", "application/xml", "application/xhtml+xml", "application/dash+xml", "video/vnd.mpeg.dash.mpd"}
		if not html_only:
			ALLOWED.add("application/dash+xml")
			ALLOWED.add("text/plain")
			ALLOWED.add("application/json")

		def encoding_detect(document):
			match = re.findall(bytes(r'\bcharset\s*=\s*"?([a-z0-9_-]+)', "ASCII"), document, re.IGNORECASE | re.MULTILINE)
			return str(match[0], "ASCII") if match else "UTF-8"

		if not headers:
			headers = {}
		parsed = urllib.parse.urlparse(url)

		code, body, response_headers = None, None, None

		if js:
			lib.log("download text with js: " + url)

			if not self.session:
				self.session = HTMLSession()
			request = self.session.get(url, headers=headers)
			request.html.render(sleep=1, keep_page=True) # TODO remove this sleep

			# TODO add hash to the location (should fix livetv.ru)
			#request.html.render(script="ar = 'alieztv|140423|1038702|1556185|55|1|es'; arr = ar.split('|');show_webplayer(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]);")
			# TODO after 5 minutes livetv.ru stops video
			# TODO it looks the videos come from http://aliez.me/live/kiet25/ so I can directly connect to that

			code = request.status_code
			body = request.html.raw_html
			response_headers = request.headers

			mime = self.content_type(response_headers)
			if mime not in ALLOWED:
				return None, {"ignore": "content type {}".format(mime)}
		else:
			client_keep = True

			entity = self._entity_name(parsed)
			while True:
				try:
					if not client:
						client = self._client(parsed.scheme, parsed.netloc)
						client_keep = False

					lib.log("GET", ("https://" if client.default_port == 443 else "http://"), client.host, client.port, entity, headers)
					client.request("GET", entity, headers=headers)
					response = client.getresponse()

					if (response.status // 100) == 3:
						response.read()
						url = response.headers["location"]
						parsed = urllib.parse.urlparse(url)
						if parsed.netloc: # netloc can be empty when redirecting to the same server
							if not client_keep:
								client.close()
							client = None
						entity = self._entity_name(parsed)
						continue

					# Make sure to not download unnecessary files (they can be big).
					mime = self.content_type(response.headers)
					if mime not in ALLOWED:
						client.close() # TODO is this fine
						return None, {"ignore": "mime {} not allowed".format(mime), "code": response.status, "headers": response.headers}

					body = response.read()
					code, response_headers = response.status, response.headers
					break
				except http.client.RemoteDisconnected:
					# TODO see if there is a better way to deal with this
					return None, {"ignore": "remote disconnect {}:{}{}".format(client.host, client.port, entity)}
				except http.client.InvalidURL:
					return None, {"ignore": "invalid url {}".format(entity)}
				except UnicodeEncodeError:
					return None, {"ignore": "invalid content encoding"}
				except Exception as ex:
					return None, {"ignore": str(ex)}

			if client and not client_keep:
				client.close()

		if code >= 400:
			lib.log("Error while downloading text: ", url, code)
			return None, {"ignore": "error {}".format(code)}

		try:
			document = str(body, encoding_detect(body))
		except:
			lib.log("error while decoding: ", url, encoding_detect(body))
			return None, {"ignore": "cannot decode as {}".format(encoding_detect(body))}

		return document, response_headers

	def youtube_stream(self, url, headers, client):
		# TODO can I use some other format for youtube? (not dash)
		# TODO distinguish streams from other videos

		query = urllib.parse.urlparse(url).query
		query = urllib.parse.parse_qs(query)
		if "v" not in query:
			return
		video_id = query["v"][0]

		document, _ = self.download_text("https://www.youtube.com/watch?v={}&gl=US&hl=en&has_verified=1&bpctr=9999999999".format(video_id), headers=headers, client=client)
		pattern = r"ytInitialPlayerResponse\s*=\s*({.+?})\s*;\s*(?:var\s+meta|</script|\n)"
		found = re.findall(pattern, document)
		try:
			info = json.loads(found[0])
			info = info["streamingData"]
			if "dashManifestUrl" in info:
				return info["dashManifestUrl"]
			elif "hlsManifestUrl" in info:
				return info["hlsManifestUrl"]
		except:
			lib.log("invalid response", url, info)
