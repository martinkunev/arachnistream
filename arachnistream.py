#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

from server import listen
import storage
import network
#import json

storage.connect()
links = storage.load()
storage.disconnect()

"""index = 0
for link in links:
	info = json.loads(link[1])
	content = info["content-type"] if "content-type" in info else link[2]
	print("http://127.0.0.1:8080/{}\t{}\t{}\n\t{}".format(index, content, link[1], link[0]))
	index += 1"""

net = network.Network()
listen(net, links)
