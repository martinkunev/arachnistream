#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Arachni Stream.

Arachni Stream is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Arachni Stream is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Arachni Stream.  If not, see <http://www.gnu.org/licenses/>.
"""

import task
import storage
import network

storage.connect()

net = network.Network()

#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.bbc.co.uk/sounds/play/live:bbc_world_service", 0)) # not enough
#net.enqueue(task.Task(task.TASK_CRAWL, "https://open.live.bbc.co.uk/mediaselector/6/select/version/2.0/mediaset/pc/vpid/bbc_world_service/format/json/jsfunc/", 0))
#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.rfi.fr/fr/", 0))
#net.enqueue(task.Task(task.TASK_CRAWL, "https://hlscat.com", 2))
#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.squidtv.net", 2))
#net.enqueue(task.Task(task.TASK_CRAWL, "http://livetv.sx/es/eventinfo/1038702_barcelona_sevilla/#webplayer_alieztv|140423|1038702|1556185|55|1|es", 0))
#net.enqueue(task.Task(task.TASK_CRAWL, "http://livetv390.me", 2))
#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.squidtv.net/europe/france/france-001.html", 1))

#net.enqueue(task.Task(task.TASK_CRAWL, "https://hlscat.com/sport_-_-_/4", 2))
#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.radio.pervii.com/en/online-playlists-m3u.htm", 2))
net.enqueue(task.Task(task.TASK_CRAWL, "https://iptvm3ufree.com/", 2))

#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.youtube.com/watch?v=SebJIjQgdjg", 0))
#net.enqueue(task.Task(task.TASK_CRAWL, "https://www.youtube.com/watch?v=sPgqEHsONK8", 0))

while net.execute_step():
	pass
exit()

storage.disconnect()

# there is a file like https://e6-ts.cdn.bg/ntv-inter/fls/ntv_1.stream/playlist.m3u8?at=43f64d699fe67c5a0f4d786fe504477f
# but I'm unable to find it

#tasks.append(task.Task(task.TASK_CRAWL, "https://nova.bg/live", 1, info={"element": "iframe"})) # TODO last argument is a hack

#tasks.append(task.Task(task.TASK_CRAWL, "https://www.squidtv.net/europe/france/france-001.html", 0))
#tasks.append(task.Task(task.TASK_CRAWL, "http://cdn.livetv390.me/webplayer.php?t=ifr&c=1554474&lang=en&eid=1038767&lid=1554474&ci=19&si=1", 1)) # livetv.ru
#tasks.append(task.Task(task.TASK_CRAWL, "https://hlscat.com/north_macedonia/mark/iptv_macedonia")) # TODO used to test 301. more work pending
